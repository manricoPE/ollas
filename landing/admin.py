from django.contrib import admin
from .models import Banner, Message, Nosotros, Ayuda
# Define the admin class


class BannerAdmin(admin.ModelAdmin):
    list_display = ('id', 'titulo', 'titulo2', 'description','banner', 'date_register',)


class NosotrosAdmin(admin.ModelAdmin):
    list_display = ('id', 'acerca_de_nosotros', 'celular', 'ubicacion', 'ciudad', 'telefono','facebook', 'linkdin', 'twitter', 'logo')


class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_name', 'email', 'message', 'date_register','phone', 'city', 'adress')
    ordering = ('id', )
    search_fields = ('user_name', 'message',)


class AyudaAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_name', 'email', 'message', 'date_register','phone', 'distrito', 'direccion')
    ordering = ('id', )
    search_fields = ('user_name', 'message',)


admin.site.register(Message, MessageAdmin)
admin.site.register(Ayuda, AyudaAdmin)
admin.site.register(Banner, BannerAdmin)
admin.site.register(Nosotros, NosotrosAdmin)

