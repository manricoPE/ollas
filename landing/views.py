from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse

from landing.models import Message, Banner, Nosotros, Ayuda


def inicio(request):
    """Vista de inicio
    :returns: Un diccionario al index.html
    """

    nosotros = Nosotros.objects.all()[0]

    if request.method == "POST":
        name = request.POST.get('name', "")
        email = request.POST.get('email', "")
        message = request.POST.get('message', "")
        phone = request.POST.get('phone', "")
        messages = Message.objects.create(user_name=name, email=email, message=message, phone=phone)
        messages.save()
        msg_html_prospect = render_to_string('mail/welcome.html', {"message": messages, 'nosotros': nosotros })
        send_mail("Hemos recibido su mensaje exitosamente! =)", '', 'ayuda@ollascomunes.com',
                  [messages.email],
                  html_message=msg_html_prospect, )

        auto_msg_html = render_to_string('mail/contact.html', {"message": messages,  'nosotros':nosotros})
        send_mail('Nuevo contacto | LANDING', '', 'ayuda@ollascomunes.com',
                  ['ayuda@ollascomunes.come'],
                  html_message=auto_msg_html, )
        return redirect("landing:inicio")

    else:
        banner = Banner.objects.filter(publicado=True)[0]
        nosotros = Nosotros.objects.all()[0]
        print(banner)

        return render(request, "index.html", {'banner':banner, 'nosotros': nosotros, })


def registrar_vecindad(request):
    """Vista de inicio
    :returns: Un diccionario al index.html
    """

    nosotros = Nosotros.objects.all()[0]

    if request.method == "POST":
        name = request.POST.get('name', "")
        email = request.POST.get('email', "")
        message = request.POST.get('message', "")
        city = request.POST.get('city', "")
        adress = request.POST.get('adress', "")
        phone = request.POST.get('phone', "")
        messages = Message.objects.create(user_name=name, email=email, message=message, phone=phone, city=city, adress=adress )
        messages.save()
        msg_html_prospect = render_to_string('mail/welcome.html', {"message": messages, 'nosotros': nosotros})
        send_mail("Hemos recibido su registro de vecindad exitosamente! =)", '', 'ayuda@ollascomunes.com',
                  [messages.email],
                  html_message=msg_html_prospect, )

        auto_msg_html = render_to_string('mail/contact.html', {"message": messages,  'nosotros':nosotros})
        send_mail('Nuevo Registro de Vecindad ', '', 'ayuda@ollascomunes.com',
                  ['ayuda@ollascomunes.com'],
                  html_message=auto_msg_html, )
        return redirect("landing:inicio")

    else:
        banner = Banner.objects.filter(publicado=True)[0]

        print(banner)

        return render(request, "contact.html", {'banner':banner, 'nosotros': nosotros, })


def ayudar(request):
    """Vista de inicio
    :returns: Un diccionario al index.html
    """

    nosotros = Nosotros.objects.all()[0]

    if request.method == "POST":
        name = request.POST.get('name', "")
        email = request.POST.get('email', "")
        message = request.POST.get('message', "")
        phone = request.POST.get('phone', "")
        messages = Ayuda.objects.create(user_name=name, email=email, message=message, phone=phone)
        messages.save()
        """msg_html_prospect = render_to_string('mail/welcome.html', {"message": messages, 'nosotros': nosotros})
        send_mail("Hemos recibido su mensaje exitosamente! =)", '', 'ayuda@ollascomunes.com',
                  [messages.email],
                  html_message=msg_html_prospect, )"""

        auto_msg_html = render_to_string('mail/contact.html', {"message": messages, 'nosotros': nosotros})
        send_mail('Una persona quiere ayudar', '', 'ayuda@ollascomunes.com',
                  ['ayuda@ollascomunes.com'],
                  html_message=auto_msg_html, )
        return redirect("landing:inicio")

    else:
        banner = Banner.objects.filter(publicado=True)[0]

        print(banner)

        return render(request, "contact.html", {'banner': banner, 'nosotros': nosotros, })


def faqs(request):
    """Vista de todos los blogs
    :returns: Un diccionario al faqs.html
    """
    nosotros = Nosotros.objects.all()[0]

    return render(request, "faqs.html", {'nosotros':nosotros, })


def terminos(request):
    """Vista de todos los blogs
    :returns: Un diccionario al terminos.html
    """
    nosotros = Nosotros.objects.all()[0]

    return render(request, "terminos.html", {'nosotros':nosotros, })