# Generated by Django 2.0 on 2020-02-06 21:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0003_auto_20200206_1601'),
    ]

    operations = [
        migrations.AddField(
            model_name='nosotros',
            name='celular',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='nosotros',
            name='ciudad',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='nosotros',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
        migrations.AddField(
            model_name='nosotros',
            name='ubicacion',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
