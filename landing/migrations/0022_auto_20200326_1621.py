# Generated by Django 2.0 on 2020-03-26 21:21

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0021_beneneficios'),
    ]

    operations = [
        migrations.CreateModel(
            name='Beneficios',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(blank=True, max_length=100, null=True)),
                ('descripcion', ckeditor.fields.RichTextField(default='')),
                ('img', models.ImageField(blank=True, help_text='el tamaño de la foto debe ser de 360 x 360 (o cuadrada, pero todos del mismo tamaño)', null=True, upload_to='landing/servicios/beneficios')),
                ('publicado', models.BooleanField(default=True, help_text='El banner debe estar publicado?')),
                ('servicio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='landing.Servicios')),
            ],
            options={
                'verbose_name': 'Beneficio',
                'verbose_name_plural': 'Beneficios',
            },
        ),
        migrations.DeleteModel(
            name='Beneneficios',
        ),
    ]
