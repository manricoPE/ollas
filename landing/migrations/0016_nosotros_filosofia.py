# Generated by Django 2.0 on 2020-03-26 03:12

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0015_auto_20200325_2116'),
    ]

    operations = [
        migrations.AddField(
            model_name='nosotros',
            name='filosofia',
            field=ckeditor.fields.RichTextField(default=''),
        ),
    ]
