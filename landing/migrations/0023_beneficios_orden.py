# Generated by Django 2.0 on 2020-03-26 22:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0022_auto_20200326_1621'),
    ]

    operations = [
        migrations.AddField(
            model_name='beneficios',
            name='orden',
            field=models.IntegerField(blank=True, default=1, null=True),
        ),
    ]
