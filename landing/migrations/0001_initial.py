# Generated by Django 2.0 on 2020-02-06 03:53

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(blank=True, max_length=40, null=True)),
                ('titulo2', models.CharField(blank=True, max_length=40, null=True)),
                ('description', models.TextField(default='')),
                ('url', models.URLField(blank=True, help_text='Colocar la URL donde quieres que te dirija al darle click a un btn en la portada', null=True)),
                ('btn', models.CharField(blank=True, max_length=40, null=True)),
                ('banner', models.ImageField(blank=True, help_text='el tamaño de la foto debe ser de 1900x1080', null=True, upload_to='landing/banners')),
                ('date_register', models.DateTimeField(auto_now=True)),
                ('publicado', models.BooleanField(default=True, help_text='El banner debe estar publicado?')),
            ],
        ),
        migrations.CreateModel(
            name='Clientes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cliente', models.CharField(blank=True, max_length=100, null=True)),
                ('orden', models.IntegerField(blank=True, null=True)),
                ('descripcion', models.CharField(blank=True, max_length=100, null=True)),
                ('img', models.ImageField(blank=True, help_text='el tamaño de la foto debe ser de 360 x 360 (o cuadrada, pero todos del mismo tamaño)', null=True, upload_to='landing/servicios')),
            ],
        ),
        migrations.CreateModel(
            name='Equipo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(blank=True, max_length=40, null=True)),
                ('apellido', models.CharField(blank=True, max_length=40, null=True)),
                ('cargo', models.CharField(blank=True, max_length=40, null=True)),
                ('descripcion', models.CharField(blank=True, help_text='es como si un tweet te describiera', max_length=200, null=True)),
                ('avatar', models.ImageField(blank=True, help_text='el tamaño de la foto debe ser de 360 x 360 (o cuadrada, pero todos del mismo tamaño', null=True, upload_to='landing/avatar')),
                ('redes', models.URLField(blank=True, help_text='Colocar la URL linkedin o alguna red importante', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_register', models.DateTimeField(auto_now=True)),
                ('user_name', models.CharField(max_length=40)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('message', models.CharField(max_length=200)),
                ('phone', models.CharField(default='', max_length=20)),
            ],
            options={
                'ordering': ('pk',),
            },
        ),
        migrations.CreateModel(
            name='Nosotros',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mision', models.TextField(default='')),
                ('vision', models.TextField(default='')),
                ('facebook', models.URLField(blank=True, help_text='Colocar la URL de facebook', null=True)),
                ('linkdin', models.URLField(blank=True, help_text='Colocar la URL de linkedin', null=True)),
                ('otra_red', models.URLField(blank=True, help_text='Colocar la URL de la red', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Servicios',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('servicio', models.CharField(blank=True, max_length=100, null=True)),
                ('icon_code', models.CharField(blank=True, max_length=40, null=True)),
                ('orden', models.IntegerField(blank=True, null=True)),
                ('descripcion', models.CharField(blank=True, max_length=100, null=True)),
                ('img', models.ImageField(blank=True, help_text='el tamaño de la foto debe ser de 360 x 360 (o cuadrada, pero todos del mismo tamaño)', null=True, upload_to='landing/servicios')),
            ],
        ),
    ]
