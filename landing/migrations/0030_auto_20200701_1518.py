# Generated by Django 2.0 on 2020-07-01 20:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0029_auto_20200701_1427'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='banner',
            name='btn',
        ),
        migrations.RemoveField(
            model_name='banner',
            name='url',
        ),
        migrations.RemoveField(
            model_name='nosotros',
            name='btn',
        ),
        migrations.RemoveField(
            model_name='nosotros',
            name='btn_url',
        ),
        migrations.RemoveField(
            model_name='nosotros',
            name='faqs',
        ),
        migrations.RemoveField(
            model_name='nosotros',
            name='filosofia',
        ),
        migrations.RemoveField(
            model_name='nosotros',
            name='historia',
        ),
        migrations.RemoveField(
            model_name='nosotros',
            name='img',
        ),
        migrations.RemoveField(
            model_name='nosotros',
            name='mision',
        ),
        migrations.RemoveField(
            model_name='nosotros',
            name='terms_cond',
        ),
        migrations.RemoveField(
            model_name='nosotros',
            name='vision',
        ),
    ]
