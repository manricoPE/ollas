"""vlogs URL Configuration
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
"""
from django.conf.urls import url
from django.conf.urls.static import static
from ollascomunes import settings
from landing import views
from django.urls import path

app_name = 'landing'
urlpatterns = [
    path('', views.inicio, name='inicio'),
    path('ayudar/', views.ayudar, name='ayudar'),
    path('registrar-vecindad/', views.registrar_vecindad, name='registrar'),
    path('faqs/', views.faqs, name='faqs'),
    path('terminos/', views.terminos, name='terminos'),

]
