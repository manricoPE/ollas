# Create your models here.
from django.db import models
from ckeditor.fields import RichTextField


# Create your models here.


class Banner(models.Model):
    titulo = models.CharField(max_length=100, blank=True, null=True)
    titulo2 = models.CharField(max_length=100, blank=True, null=True)
    description = RichTextField(default="")
    banner = models.ImageField(upload_to='landing/banners', blank=True, null=True,
                               help_text="el tamaño de la foto debe ser de 1900x1080")
    date_register = models.DateTimeField(auto_now=True, blank=True)
    publicado = models.BooleanField(default=True, help_text="El banner debe estar publicado?")

    def __str__(self):
        if self.id is None:
            return "banner"
        else:
            return "banner" + str(self.id)

    class Meta:
        verbose_name = "Portada Principal"
        verbose_name_plural = "Portadas"


class Nosotros(models.Model):

    motivo_de_vida = models.CharField(max_length=200, blank=True, null=True)
    logo = models.ImageField(upload_to='landing', blank=True, null=True)
    acerca_de_nosotros = models.TextField(default="")
    celular = models.IntegerField(blank=True, null=True)
    telefono = models.IntegerField(blank=True, null=True)
    ubicacion = models.CharField(max_length=100, blank=True, null=True)
    ciudad = models.CharField(max_length=50, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    facebook = models.URLField(null=True, blank=True, help_text="Colocar la URL de facebook")
    linkdin = models.URLField(null=True, blank=True, help_text="Colocar la URL de linkedin")
    twitter = models.URLField(null=True, blank=True, help_text="Colocar la URL de la red")


    def __str__(self):
        return self.motivo_de_vida

    class Meta:
        verbose_name = "Acerca de la Empresa"
        verbose_name_plural = "Acerca de la Empresa"


class Message(models.Model):
    date_register = models.DateTimeField(auto_now=True, blank=True)
    user_name = models.CharField(max_length=40, verbose_name="Representante o Contacto")
    email = models.EmailField(blank=True, null=True, verbose_name="Correo")
    message = models.CharField(max_length=200, verbose_name="Mensaje")
    phone = models.CharField(max_length=20, default="", verbose_name="Celular")
    city = models.CharField(max_length=20, default="", verbose_name="Disrito")
    adress = models.CharField(max_length=20, default="", verbose_name="Direccion de la Localidad")

    class Meta:
        ordering = ('pk',)
        verbose_name = "Vecindad"
        verbose_name_plural = "Vecindades"

    def __str__(self):
        return str(self.pk)


class Ayuda(models.Model):
    date_register = models.DateTimeField(auto_now=True, blank=True)
    user_name = models.CharField(max_length=40, verbose_name="Nombre de la Persona que quiere ayudar")
    email = models.EmailField(blank=True, null=True, verbose_name="Correo")
    message = models.CharField(max_length=200, verbose_name="Mensaje", blank=True, null=True)
    phone = models.CharField(max_length=20, default="", verbose_name="Celular")
    distrito = models.CharField(max_length=20, default="", blank=True, null=True, verbose_name="Disrito")
    direccion = models.CharField(max_length=20, default="", blank=True, null=True,
                                 verbose_name="Direccion de la Localidad")

    class Meta:
        ordering = ('pk',)
        verbose_name = "Ayuda"
        verbose_name_plural = "Ayuda Solidaria"

    def __str__(self):
        return str(self.pk)
